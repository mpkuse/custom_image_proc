ROS-Custom Image Processing Node
---------------------------------

Purpose:
Custom Image Processing on a input stream. Useful for resizing etc. 

Remap the subscriber, publisher topic (see launch file)

Subscribes to /in (sensor_msgs::Image)
Publishes to /out (sensor_msgs::Image)


Contact :
Manohar Kuse <mpkuse@connect.ust.hk>
29th Feb, 2016


