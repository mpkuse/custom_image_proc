#include <ros/ros.h>
#include <image_transport/image_transport.h>


#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

using namespace std;

image_transport::Subscriber sub;
image_transport::Publisher pub;
int nTimesDownSample = 2; //should be 1,2,3,4,5 (do not set it to zero)

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    cv::Mat inputImage = cv_bridge::toCvShare(msg)->image;
    cv::Mat inputImageGray;

    if( inputImage.channels() != 1 )
        cvtColor( inputImage, inputImageGray, CV_BGR2GRAY );
    else
        inputImageGray = inputImage.clone();

    //
    // Do processing
    cv::Mat dst;
    int s = nTimesDownSample;

    if( s>0 )
    {
    //pyrDown( inputImageGray, dst, cv::Size( inputImage.cols/s, inputImage.rows/s ) );
    pyrDown( inputImageGray, dst );
    for( int i=0 ; i<(s-1) ; i++ )
        pyrDown( dst, dst );

    }
    else
    {
        ROS_INFO_ONCE( "Full resolution" );
        dst = inputImageGray.clone();
    }

    //
    // Publish `dst`
    sensor_msgs::ImagePtr msgGo;
    msgGo = cv_bridge::CvImage(msg->header, "mono8", dst).toImageMsg();


    pub.publish( msgGo );


//    cv::imshow("view", inputImage );
//    cv::imshow( "view", dst );
//    cv::waitKey(30);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'mono8'.", msg->encoding.c_str());
  }
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "custom_image_proc_node");
  ros::NodeHandle nh("~");

  int down_sample_times = -1;
  if( nh.getParam( "down_sample_times" , down_sample_times ) )
  {
      cout << "down sample # : "<< down_sample_times << endl;
      nTimesDownSample = down_sample_times;
  }
  else
      cout << "default downsampling\n";

//  cv::namedWindow("view");
//  cv::startWindowThread();
  image_transport::ImageTransport it(nh);
  sub = it.subscribe("/in", 1, imageCallback);

  pub = it.advertise("/out", 1);

  ros::Rate r(100);
  while( ros::ok() )
  {
      ros::spinOnce();
      r.sleep();
  }

}
